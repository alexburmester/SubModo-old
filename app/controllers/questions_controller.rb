class QuestionsController < ApplicationController
  before_action :set_question, only: [:show, :edit, :update, :destroy]
  respond_to :html, :json
  
  def index
    @questions = Question.order("created_at DESC")
    @meta_title = meta_title("Nyeste Spørgsmål")
    @meta_description = "Spørg gratis og anonymt advokater og andre juridiske eksperter om råd og vejledning"
  end

  def show
    @answer = Answer.new
    @meta_title = meta_title(@question.title)
    @meta_description = @question.body[0..160]

    respond_with_question_or_redirect
  end

  def new
    @question = Question.new
    @meta_title = meta_title("Stil Spørgsmål")
  end

  def create
    @question = Question.new(question_params)

    respond_to do |format|
      if @question.save
        format.html { redirect_to @question, notice: 'Dit spørgsmål er nu oprettet.' }
        format.json { render :show, status: :created, location: @question }
      else
        format.html { render :new }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @question.destroy
    respond_to do |format|
      format.html { redirect_to questions_url, notice: 'Spørgsmålet er slettet.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_question
      @question = Question.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def question_params
      params.require(:question).permit(:title, :body)
    end

    def respond_with_question_or_redirect
      if request.path != question_path(@question)
        return redirect_to @question, status: :moved_permanently
      else
        return respond_with @question
      end
    end
end
