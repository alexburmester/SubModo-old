class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  BRAND_NAME = "SubModo".freeze

  def meta_title(title)
  	[title, BRAND_NAME].reject(&:empty?).join(" | ")
  end


  protected

  # Overrides default devise strong parameters to allow
  # input of name and occupation at registration/profile edit pages
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :occupation])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :occupation])
  end
end
