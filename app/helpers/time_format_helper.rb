module TimeFormatHelper
	
	# accepts an active record object
	# returns a formatted version of the objects creation time
	# for use in the question/answer sidebar
	def creation_time(time)
		time.created_at.strftime("%d/%m/%Y - %H:%M")
	end
end