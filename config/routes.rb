Rails.application.routes.draw do

	root "questions#index"

  devise_for :users, controllers: {
  	sessions: "users/sessions"
  }

  resources :questions do
  	resources :answers 
  end

  get "about", to: "pages#about"
  get "contact", to: "pages#contact"
  get "terms", to: "pages#terms"
  
end